package com.loginsession.demologinsession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemologinsessionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemologinsessionApplication.class, args);
	}

}
